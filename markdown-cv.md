# Mario Karam 
######  Mechanical engineer
![](https://ae01.alicdn.com/kf/HTB17p1fNVXXXXXHaXXXq6xXFXXXX/Super-Mario-Bros-Gra-Dla-Dzieci-S-odkie-Tkaniny-plakat-21-x-13-Wystr-j-01.jpg_640x640.jpg)

### Contact Info:
- Phone : 71/257345
- Email : mariokaram257@gmail.com

## Skills
### 
- C++
- html
- javascript
- Pyhton
- Autocad


## Languages
| Language    | Writing/Reading 	| Speech 	|
| ------------- |:-----------------:| -------:|
| **French**    | Good/Primary  	| Excellent |
| **English**   | Good/Secondary    | Good  	|
| **Arabic** 		| Primary	      	| Fair  	|

### Experience
--------------
| Position |  Company   | Location | Year     |
| -------- |----------:|---------:| --------:|
| Site engineer   | Engineering	S| Lebanon	| 2016-2018|

### School 
-------------
| Field    		   | School | Location  | Year 	  |
| ---------------  |:-----------------:|----------:| --------:|
| General science|frere|Lebanon|1993-2011|
